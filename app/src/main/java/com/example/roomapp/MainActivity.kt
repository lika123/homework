package com.example.roomapp

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import androidx.room.Room
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val db by lazy {
        Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "database-name"
        ).build()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun save(view: View) {
        AsyncTask.execute {
            val user = User()
            user.firstName = firstNameEditText.text.toString()
            user.lastName = lastNameEditText.text.toString()

            db.userDao().insertAll(user)
            d("addedUser", "$user")

        }
    }

    fun read(view: View) {
        AsyncTask.execute {
            val users = db.userDao().getAll()
            val size = users.size
            d("userCount", "$size")
            for (user in users){
                d("useFirstName", "${user.firstName}")
            }
        }
    }

    fun delete(view: View) {
        AsyncTask.execute {
            val user = User()
            val users = db.userDao().getAll()
            user.firstName = firstNameEditText.text.toString()
            user.lastName = lastNameEditText.text.toString()
            for (it in users)
                if (user.uid == it.uid)
                    db.userDao().delete(user)
                    d("deleteUser", "$user")
        }
    }


}
